class ListItem extends React.Component {

    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick () {
        const {
            title,
            removeItemCallBack,
        } = this.props
        removeItemCallBack(title)
    }
    
    render() {
        const {
            title,
            removeItemCallBack,
        } = this.props


        return (
            <li onClick={removeItemCallBack}>{title}</li>
        )
    }
}