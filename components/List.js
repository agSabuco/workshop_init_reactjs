class List extends React.Component {

    handleClick2() {
        const {
            items,
            value,
            removeItemCallBack
        } = this.state

        console.log(value)
    }

    render() {
        const {
            items,
        } = this.props

        return (
            <ul>
                {items.map(item => <ListItem key={item} title={item} removeItemCallBack={removeItemCallBack}/>)}
            </ul>
        )
    }
}