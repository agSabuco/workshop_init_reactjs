class Button extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false,
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick() {
        const {
            active,
        } = this.state

        this.setState({ active: !active })
    }


    render() {
        const {
            title,
        } = this.props

        const {
            active,
        } = this.state

        return (

                <button className={active ? 'active' : ''} onClick={this.handleClick}> {title} {active ? 'Active' : 'Inactive'} </button>
            
        )
    }
}