class Todo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [],
            value: '',
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleClick2 = this.handleClick2.bind(this)
    }

    handleChange(event) {
        this.setState({
            value: event.target.value,
        })
    }

    handleClick() {
        const {
            items,
            value,
        } = this.state

        items.push(value)

        this.setState({
            items,
            value: '',
        })

        
    }

    handleClick2(title) {
        const {
            items,
            value,
        } = this.state

        console.log(value)
    }

    render() {
        const {
            items,
            value,
        } = this.state
        return (
            
            <div className="todo">
                <List removeItemCallBack={this.handleClick2} items={items}/> 
                <div className="actions">
                    <input type="text" placeholder="escribe algo..." value={value} onChange={this.handleChange}/>
                    <button onClick={this.handleClick}>Add</button>   
                </div>            
            </div>
        )
    }
}

//Lista
//Items de la Lista
// input
//botton add